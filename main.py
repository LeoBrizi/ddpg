import argparse
from tqdm import tqdm
from collections import deque
import random
import os
import shutil

import numpy as np
import mujoco_py
import gym
import tensorflow as tf

from tensorflow.python.framework import ops


from Actor import *
from Critic import *
from OU_Noise import *
from G_Noise import *

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

VERSION = "V3"

# function to organize the selected elements from the batch


def batch_selector(batch):
    states = np.array([i[0] for i in batch])
    actions = np.array([i[1] for i in batch])
    rews = np.array([i[2] for i in batch])
    nextStates = np.array([i[3] for i in batch])
    dones = np.array([i[4] for i in batch])
    return states, actions, rews, nextStates, dones

def initialize_replay_buffer(bufferReplay):
    state = env.reset()
    for i in range(bufferSize):
        action = env.action_space.sample()
        nextState, rew, done, _ = env.step(action)
        bufferReplay.append((state, action, rew, nextState, done))
        if(done):
            state = env.reset()
        else:
            state = nextState

def DDPG():
    initialize_replay_buffer(bufferReplay)
    print("the replay buffer is initialized with random action")
    for episode in (range(numEpisodes)):
        state = env.reset()
        steps = 0
        totReward = 0.0
        for step in (range(maxSteps)):
            # action with noise. 
            action = actor.predict([state]) + 1.0 / \
                (1.0 + actionNoise * episode) * noise.sample() #select an action
            #action = actor.predict([state]) + noise.sample()

            nextState, rew, done, _ = env.step(action[0]) #execute the action

            bufferReplay.append((state, action[0], rew, nextState, done)) #add the transition into the replay buffer

            state = nextState
            # if the buffer is full, forget the oldest experience
            if len(bufferReplay) >= bufferSize:
                bufferReplay.popleft()

            # sample a random batch of experiences from the replay buffer
            batch = random.sample(bufferReplay, batchSize)
            bStates, bActions, bRews, bNextStates, bDones = batch_selector(
                batch)

            # produce action and q-val from the target networks
            actionTarget = actor.target_predict(bNextStates)                #mu'(si+1)
            qTarget = critic.target_predict(bNextStates, actionTarget)      #Q'(si+1,mu'(si+1))
            y = np.zeros(batchSize)
            # use the Bellman equation
            for i in range(batchSize):
                if bDones[i]:
                    y[i] = bRews[i]#only the reward
                else:
                    y[i] = bRews[i] + gamma * qTarget[i]
            # critic train, minimize the MSBE
            y = np.reshape(y, (batchSize, 1)) #MSBE mean squared Bellman error
            critic.train(bStates, bActions, y) 
            # actor train. Update the policy using the gradient of the Q
            predActions = actor.predict(bStates)
            gradient = critic.gradient(bStates, predActions)
            actor.train(bStates, gradient[0])
            # update softly the target networks
            actor.target_train()
            critic.target_train()
            steps += 1
            totReward += rew
            if done:
                break
        # collect stats
        stepsVec.append(steps)
        rewVec.append(totReward)
        meanSteps = np.mean(stepsVec[-100:])
        meanRew = np.mean(rewVec[-100:])
        print("Train_ep: {} | Ep_reward: {} | Last_mean_reward {} ".format(
            episode, round(totReward, 2), round(meanRew, 2)), end="\r")
        # write on the tensorboard
        summary_ = sess.run(summary, feed_dict={episodeReward: totReward})
        writer.add_summary(summary_, episode)
        writer.flush()
        # save networks
        if episode % 100 == 0:
            print("save the network")
            saver.save(sess, networkDir + "/" + "ddpg")


def test_policy():
    # just run the policy on the environment
    state = env.reset()
    totalRew = 0
    for episode in range(numEpisodes):
        for step in range(maxSteps):
            env.render()
            action = actor.predict([state])
            nextState, rew, done, _ = env.step(action[0])
            totalRew += rew
            state = nextState
            if done:
                state = env.reset()
                print("Episode: {} | Reward: {} \r".format(
                    episode, round(totalRew, 3)))
                totalRew = 0
                break

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--env', help='which environment to use', default='Ant-v2')
    parser.add_argument('--mode', help='train or test', default='train')
    parser.add_argument(
        '--numEpisodes', help='how main episodes to train', default=10000)
    parser.add_argument(
        '--bufferReplay', help='replay buffer size', default=1000000)

    args = parser.parse_args()

    env = gym.make(args.env)

    # create the folder to store the saved networks
    networkDir = "networks/" + args.env + VERSION
    if not os.path.exists(networkDir):
        os.mkdir(networkDir)
        print('Directory created for the env ', args.env)
    else:
        print('Directory already exists')

    # to reset the tensorboard stuff
    statsDir = "stats/" + args.env + VERSION + "/"
    if not os.path.exists(statsDir):
        os.mkdir(statsDir)
    else:
        shutil.rmtree(statsDir)
        os.mkdir(statsDir)

    # parameters
    stateSize = env.observation_space.shape[0]
    actionSize = env.action_space.shape[0]

    numEpisodes = int(args.numEpisodes)
    maxSteps = 5000
    bufferSize = int(args.bufferReplay)
    batchSize = 100
    gamma = 0.99
    bufferReplay = deque()
    actionNoise = 0.02
    polyakAveraging = 0.005
    criticLearningRate = 0.001
    actorLearningRate = 0.001

    # tensorflow session
    sess = tf.Session()
    actor = Actor(actionSize, stateSize, batchSize, polyakAveraging, actorLearningRate, sess)
    critic = Critic(actionSize, stateSize, actor.numTrainableVars, polyakAveraging, criticLearningRate, sess)
    saver = tf.train.Saver()

    noise = OU_Noise(actionSize)
    #noise = G_Noise(actionSize)

    # tensorboard stuff
    writer = tf.summary.FileWriter(statsDir, sess.graph)
    episodeReward = tf.Variable(0.)
    tf.summary.scalar("Reward", episodeReward)
    summary = tf.summary.merge_all()

    # net initialization
    sess.run(tf.global_variables_initializer())
    checkpoint = tf.train.get_checkpoint_state(networkDir)
    stepsVec = []
    rewVec = []
    print("environment: ",args.env)
    print("episodes: ",args.numEpisodes)
    # restore networks after a training, if possible
    if checkpoint and checkpoint.model_checkpoint_path:
        saver.restore(sess, checkpoint.model_checkpoint_path)
        print("Successfully loaded:", checkpoint.model_checkpoint_path)
    else:
        print("Could not find old network weights")

    if args.mode == 'train':
        DDPG()
    else:
        test_policy()

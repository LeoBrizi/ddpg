import numpy as np
import random


class G_Noise(object):
    '''produces a noise that has an inertia Ornstein-Uhlenbeck Noise it depends by the prevoius step'''

    def __init__(self, actionDim, mu=0, sigma=0.1):
        self.dim = actionDim
        self.mu = mu
        self.sigma = sigma

    def sample(self):
        return np.random.normal(self.mu, self.sigma, self.dim)


import numpy as np
import random


class OU_Noise(object):
    '''produces a noise that has an inertia Ornstein-Uhlenbeck Noise it depends by the prevoius step'''

    def __init__(self, actionDim, mu=0, theta=0.15, sigma=0.3):
        self.dim = actionDim
        self.mu = mu
        self.theta = theta
        self.sigma = sigma
        self.state = np.ones(actionDim) * mu

    def sample(self):
        x = self.state
        dx = self.theta * (self.mu - x) + self.sigma * \
            np.random.uniform(low=-1, high=1, size=(len(x)))
        self.state = x + dx
        return self.state

    def reset(self):
        self.state = np.ones(self.dim) * self.mu

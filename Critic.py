import tensorflow as tf


class Critic():

    def __init__(self, actSize, stateSize, numActVars, polyakAveraging, criticLearningRate, sess):
        '''hidden layer size: 400x300'''

        self.inputSize = stateSize
        self.actSize = actSize
        self.sess = sess

        self.input = tf.placeholder(
            shape=[None, self.inputSize], dtype=tf.float32)
        self.target = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        self.action = tf.placeholder(
            shape=[None, self.actSize], dtype=tf.float32)

        # create critic net
        self.W1 = tf.Variable(tf.truncated_normal(
            [self.inputSize, 400], stddev=0.01))
        self.b1 = tf.Variable(tf.zeros(400))

        self.W2 = tf.Variable(tf.truncated_normal(
            [self.actSize, 300], stddev=0.01))
        self.b2 = tf.Variable(tf.zeros(300))

        self.W3 = tf.Variable(tf.truncated_normal([400, 300], stddev=0.01))
        self.b3 = tf.Variable(tf.zeros(300))

        self.W4 = tf.Variable(tf.truncated_normal([300, 1], stddev=0.003))
        self.b4 = tf.Variable(tf.zeros(1))

        self.out1 = tf.nn.relu(tf.matmul(self.input, self.W1) + self.b1)
        # the actions are embedded before the second hidden layer
        self.out2 = tf.nn.relu(
            tf.matmul(self.out1, self.W3) + tf.matmul(self.action, self.W2) + self.b2)
        self.out = tf.matmul(self.out2, self.W4) + self.b4

        # store the parameters (for compute the gradient)
        self.networkParams = tf.trainable_variables()[numActVars:]


        # create target critic net
        self.W1T = tf.Variable(tf.truncated_normal(
            [self.inputSize, 400], stddev=0.01))
        self.b1T = tf.Variable(tf.zeros(400))

        self.W2T = tf.Variable(tf.truncated_normal(
            [self.actSize, 300], stddev=0.01))
        self.b2T = tf.Variable(tf.zeros(300))

        self.W3T = tf.Variable(tf.truncated_normal([400, 300], stddev=0.01))
        self.b3T = tf.Variable(tf.zeros(300))

        self.W4T = tf.Variable(tf.truncated_normal([300, 1], stddev=0.003))
        self.b4T = tf.Variable(tf.zeros(1))

        self.out1T = tf.nn.relu(
            tf.matmul(self.input, self.W1T) + self.b1T)
        self.out2T = tf.nn.relu(tf.matmul(
            self.out1T, self.W3T) + tf.matmul(self.action, self.W2T) + self.b2T)
        self.outT = tf.matmul(self.out2T, self.W4T) + self.b4T

        self.targetNetworkParams = tf.trainable_variables(
        )[(len(self.networkParams) + numActVars):]

        # update target net weights
        self.updateTargetNetworkParams = \
            [self.targetNetworkParams[i].assign(tf.multiply(self.networkParams[i], polyakAveraging) +
                                                  tf.multiply(self.targetNetworkParams[i], 1. - polyakAveraging))#polyak averaging try with 0.005
                for i in range(len(self.targetNetworkParams))]

        # define the loss and train
        self.loss = tf.reduce_mean(tf.square(self.target - self.out))
        self.optimize = tf.train.AdamOptimizer(criticLearningRate).minimize(self.loss)#critic learning rate
        self.actionGrads = tf.gradients(self.out, self.action)

    def predict(self, state, action):
        return self.sess.run(self.out, feed_dict={self.input: state, self.action: action})

    def target_predict(self, state, action):
        return self.sess.run(self.outT, feed_dict={self.input: state, self.action: action})

    def gradient(self, state, action):
        return self.sess.run(self.actionGrads, feed_dict={self.input: state, self.action: action})

    def train(self, state, action, target): #target=Y
        self.sess.run(self.optimize, feed_dict={
                      self.input: state, self.action: action, self.target: target})

    def target_train(self):
        self.sess.run(self.updateTargetNetworkParams)

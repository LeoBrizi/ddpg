import tensorflow as tf


class Actor():

    def __init__(self, actSize, stateSize, batchSize, polyakAveraging, actorLearningRate, sess):
        '''hidden layer size: 400x300'''
        self.inputSize = stateSize
        self.outputSize = actSize
        self.sess = sess
        self.batchSize = batchSize

        self.input = tf.placeholder(
            shape=[None, self.inputSize], dtype=tf.float32)

        # create actor net
        self.W1 = tf.Variable(tf.truncated_normal(
            [self.inputSize, 400], stddev=0.01))
        self.b1 = tf.Variable(tf.zeros(400))

        self.W2 = tf.Variable(tf.truncated_normal([400, 300], stddev=0.01))
        self.b2 = tf.Variable(tf.zeros(300))

        self.W3 = tf.Variable(tf.truncated_normal(
            [300, self.outputSize], stddev=0.003))
        self.b3 = tf.Variable(tf.zeros(self.outputSize))

        # store the parameters of the actor (for compute the gradient)
        self.networkParams = tf.trainable_variables()

        self.out1 = tf.nn.relu(tf.matmul(self.input, self.W1) + self.b1)
        self.out2 = tf.nn.relu(tf.matmul(self.out1, self.W2) + self.b2)
        self.out = tf.nn.tanh(tf.matmul(self.out2, self.W3) + self.b3) #to bound action


        # create target actor net
        self.W1T = tf.Variable(tf.truncated_normal(
            [self.inputSize, 400], stddev=0.01))
        self.b1T = tf.Variable(tf.zeros(400))

        self.W2T = tf.Variable(tf.truncated_normal([400, 300], stddev=0.01))
        self.b2T = tf.Variable(tf.zeros(300))

        self.W3T = tf.Variable(tf.truncated_normal(
            [300, self.outputSize], stddev=0.003))
        self.b3T = tf.Variable(tf.zeros(self.outputSize))

        self.targetNetworkParams = tf.trainable_variables()[
            len(self.networkParams):]

        self.out1T = tf.nn.relu(
            tf.matmul(self.input, self.W1T) + self.b1T)
        self.out2T = tf.nn.relu(
            tf.matmul(self.out1T, self.W2T) + self.b2T)
        self.outT = tf.nn.tanh(
            tf.matmul(self.out2T, self.W3T) + self.b3T)

        # update the target network weights
        self.updateTargetNetworkParams = \
            [self.targetNetworkParams[i].assign(tf.multiply(self.networkParams[i], polyakAveraging) +
                                                  tf.multiply(self.targetNetworkParams[i], 1. - polyakAveraging))#polyak averaging try with 0.005
                for i in range(len(self.targetNetworkParams))]

        #  compute the actor gradients
        self.actionGrad = tf.placeholder(tf.float32, [None, self.outputSize])

        self.unnormalizedActorGradients = tf.gradients(
            self.out, self.networkParams, -self.actionGrad)
        self.actorGrads = list(map(lambda x: tf.div(
            x, self.batchSize), self.unnormalizedActorGradients))

        # apply the gradient
        self.optimize = tf.train.AdamOptimizer(actorLearningRate).apply_gradients( 
            zip(self.actorGrads, self.networkParams))

        self.numTrainableVars = len(
            self.networkParams) + len(self.targetNetworkParams)

    def get_num_trainable_vars(self):
        return self.numTrainableVars

    def predict(self, state):
        return self.sess.run(self.out, feed_dict={self.input: state})

    def target_predict(self, state):
        return self.sess.run(self.outT, feed_dict={self.input: state})

    def train(self, states, gradient):
        self.sess.run(self.optimize, feed_dict={
                      self.input: states, self.actionGrad: gradient})

    def target_train(self):
        self.sess.run(self.updateTargetNetworkParams)
